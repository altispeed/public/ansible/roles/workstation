<img style="float: left;" src="docs/assets/logo.png" alt="logo" width="300"/>

## <div style="text-align: right"></div>
Workstation Deployment

## Table of content

- [Default Variables](#default-variables)
  - [workstation_companion_branch](#workstation_companion_branch)
  - [workstation_companion_database_file](#workstation_companion_database_file)
  - [workstation_companion_database_root](#workstation_companion_database_root)
  - [workstation_companion_minor_version](#workstation_companion_minor_version)
  - [workstation_companion_version](#workstation_companion_version)
  - [workstation_default_autostarts](#workstation_default_autostarts)
  - [workstation_packages](#workstation_packages)
  - [workstation_streaming_obs_assets](#workstation_streaming_obs_assets)
  - [workstation_streaming_obs_config](#workstation_streaming_obs_config)
  - [workstation_usbguard](#workstation_usbguard)
  - [workstation_user](#workstation_user)
  - [workstation_user_password](#workstation_user_password)
  - [workstation_yubikey](#workstation_yubikey)
- [Discovered Tags](#discovered-tags)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---



## Default Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):


### workstation_companion_branch

#### Default Value

```YAML
workstation_companion_branch: stable
```

### workstation_companion_database_file

#### Default Value

```YAML
workstation_companion_database_file: ''
```

### workstation_companion_database_root

#### Default Value

```YAML
workstation_companion_database_root: ''
```

### workstation_companion_minor_version

#### Default Value

```YAML
workstation_companion_minor_version: 2
```

### workstation_companion_version

#### Default Value

```YAML
workstation_companion_version: 3.2
```

### workstation_default_autostarts

#### Default Value

```YAML
workstation_default_autostarts:
  - name: Firefox
    filename: firefox
    exec: snap run firefox
```

### workstation_packages

#### Default Value

```YAML
workstation_packages:
  - base
  - vpn
  - laptop
  - media
  - virtualization
  - development
  - messaging
  - medical
```

### workstation_streaming_obs_assets

#### Default Value

```YAML
workstation_streaming_obs_assets:
  src: streaming-profiles/default/assets
  dest: /home/{{ workstation_user }}/broadcast
```

### workstation_streaming_obs_config

#### Default Value

```YAML
workstation_streaming_obs_config: streaming-profiles/default/configs/obs
```

### workstation_usbguard

#### Default Value

```YAML
workstation_usbguard:
  new: false
  add: false
```

### workstation_user

#### Default Value

```YAML
workstation_user: localadmin
```

### workstation_user_password

#### Default Value

```YAML
workstation_user_password: changem3
```

### workstation_yubikey

#### Default Value

```YAML
workstation_yubikey:
  pin:
    old: '123456'
    new: '123456'
  generate: false
```

## Discovered Tags

**_always_**

**_setup-all_**

**_setup-workstation_**


## Dependencies

None.

## License

GPL-3.0-only

## Author

Dark Decoy

