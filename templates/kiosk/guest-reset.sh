#!/bin/bash

/usr/bin/btrfs subvolume delete /home/{{ workstation_user }}
/usr/bin/btrfs subvolume snapshot /backup/{{ workstation_user }}/ /home/{{ workstation_user }}
